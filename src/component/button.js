import React from 'react';
import {ScrollView,Image,StyleSheet,TouchableOpacity} from 'react-native';

const button = (props) => {
    return(
        <ScrollView horizontal>
            <TouchableOpacity onPress={props.onButtonPress}>
                <Image source={require('../assets/like.jpg')} style={styles.suka}/>
            </TouchableOpacity>
            <TouchableOpacity>
                <Image source={require('../assets/chat.jpg')} style={{width:30, height:30, marginLeft:1, marginTop:1}}/>
            </TouchableOpacity>
            <TouchableOpacity>
                <Image source={require('../assets/bagi.png')} style={{width:30, height:30, marginLeft:1, marginTop:1}}/>
            </TouchableOpacity>
            <TouchableOpacity>
                <Image source={require('../assets/simpan.jpg')} style={{width:30, height:30, marginLeft:120, marginTop:1}}/>
            </TouchableOpacity>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    suka:{
        width: 30,
        height: 30,
        marginLeft: 10,
        marginTop: 1,
    },
});

export default button;